import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  passVisible = false;
  textVisible = "Exibir";
  formValid = true;

  constructor() { 
  }

  ngOnInit() {
  }

  login($event){
    // if(!email)
    //   email = "teste@teste.com";

    // if(!password)
    //   password = "teste";


    // let user = {
    //   usrEmail: email,
    //   usrPassword: password
    // };
    let btn = $event.target;
    
    let email = btn.parentElement.childNodes[1].childNodes[1].value;
    let pass = btn.parentElement.childNodes[2].childNodes[1].value;

    if (!email || !pass) {
      this.formValid = false;
      return false;
    }

    console.log('user: '+email+' password: '+pass);
  }

  togglePassVisible(event) {
    event.target.classList.toggle('icn-visible');

    event.target.classList.toggle('icn-invisible');

    this.textVisible = (this.textVisible == "Exibir") ? "Esconder" : "Exibir";

    this.passVisible = !this.passVisible;
  }
}

// http://52.22.250.58:57440/api/Login