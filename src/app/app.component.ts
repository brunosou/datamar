import { Component, OnInit } from '@angular/core';

import { FirebasePushService } from './shared';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  message;

  constructor(
    private firebasePushService: FirebasePushService
  ) { }

  ngOnInit() {
    const userId = "0001";

    this.firebasePushService.requestPermission(userId);

    this.firebasePushService.receiveMessage();

    this.message = this.firebasePushService.currentMessage;
  }
}