import { Injectable } from '@angular/core';

import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireMessaging } from '@angular/fire/messaging';

import { take } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FirebasePushService {
  currentMessage = new BehaviorSubject(null);

  constructor (
    private angularFireData: AngularFireDatabase,
    private angularFireAuth: AngularFireAuth,
    private angularFireMessage: AngularFireMessaging
  ) {
    this.angularFireMessage.messaging.subscribe((_messaging) => {
      _messaging.onMessage = _messaging.onMessage.bind(_messaging);

      _messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
    });
  }

  updateToken(userId, token) {
    this.angularFireAuth.authState.pipe(take(1)).subscribe(() => {
      const data = {};

      data[userId] = token;

      this.angularFireData.object('fcmTokens/').update(data);
    });
  }

  requestPermission(userId) {
    this.angularFireMessage.requestToken.subscribe((token) => {
      this.updateToken(userId, token);
    }, (err) => {
      console.error('Unable to get permission to notify. ', err);
    });
  }

  receiveMessage() {
    this.angularFireMessage.messages.subscribe((payload) => {
      console.log("new message received. ", payload);

      this.currentMessage.next(payload);
    });
  }
}
