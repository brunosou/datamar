import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppRoutingModule } from '../app-routing.module';

import {
  DashboardComponent,
  PanelComponent,
  UsersComponent,
} from './';

@NgModule({
  imports: [
    AppRoutingModule,
    CommonModule,
  ],
  declarations: [
    DashboardComponent,
    PanelComponent,
    UsersComponent,
  ]
})
export class PanelModule { }
